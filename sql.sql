/*
 Navicat MySQL Data Transfer

 Source Server         : 腾讯高可用版
 Source Server Type    : MySQL
 Source Server Version : 50718
 Source Host           : 59507f211162e.gz.cdb.myqcloud.com:17014
 Source Schema         : vip_chunabao_cn

 Target Server Type    : MySQL
 Target Server Version : 50718
 File Encoding         : 65001

 Date: 23/11/2019 00:32:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hq_ad
-- ----------------------------
DROP TABLE IF EXISTS `hq_ad`;
CREATE TABLE `hq_ad`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userid` smallint(8) NULL DEFAULT NULL COMMENT '用户ID',
  `category` smallint(5) NOT NULL DEFAULT 0 COMMENT '分类',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '描述',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '链接',
  `target` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '打开方式',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `sort_order` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '广告' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_ad
-- ----------------------------
INSERT INTO `hq_ad` VALUES (1, NULL, 4, '12132', '111', '111', '_self', 'https://vip.chunabao.cn/uploads/20191122/609b85504372c224ef5a337dfaa7941c.jpg', 0, 1, 0, 0, 0);
INSERT INTO `hq_ad` VALUES (2, NULL, 5, 'sfasdfas', '', '', '_self', '', 12, 1, 0, 0, 0);

-- ----------------------------
-- Table structure for hq_ad_model
-- ----------------------------
DROP TABLE IF EXISTS `hq_ad_model`;
CREATE TABLE `hq_ad_model`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(10) NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_ad_model
-- ----------------------------
INSERT INTO `hq_ad_model` VALUES (4, '首页', 1574394319, 1574394319, 0);
INSERT INTO `hq_ad_model` VALUES (5, '列表banner', 1574394336, 1574396950, 0);

-- ----------------------------
-- Table structure for hq_admin
-- ----------------------------
DROP TABLE IF EXISTS `hq_admin`;
CREATE TABLE `hq_admin`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员用户名',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员密码',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0禁用/1启动',
  `last_login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上次登录时间',
  `last_login_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '上次登录IP',
  `login_count` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '登录次数',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_admin
-- ----------------------------
INSERT INTO `hq_admin` VALUES (1, 'hqs316', 'e10adc3949ba59abbe56e057f20f883e', 1, 1574439151, '220.200.63.67', 116, 1555249039, 1574439151);
INSERT INTO `hq_admin` VALUES (27, 'huangqing', 'e10adc3949ba59abbe56e057f20f883e', 1, 1574438053, '117.136.38.54', 17, 1574304401, 1574438230);

-- ----------------------------
-- Table structure for hq_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `hq_admin_log`;
CREATE TABLE `hq_admin_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员id',
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员用户名',
  `useragent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'User-Agent',
  `ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'ip地址',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求链接',
  `method` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求类型',
  `type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '资源类型',
  `param` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求参数',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '日志备注',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 751 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for hq_article
-- ----------------------------
DROP TABLE IF EXISTS `hq_article`;
CREATE TABLE `hq_article`  (
  `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userid` int(8) NULL DEFAULT NULL COMMENT '用户ID',
  `cid` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类ID',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '图片',
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '作者',
  `summary` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '简介',
  `photo` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '相册',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `view` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '点击量',
  `is_top` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否置顶',
  `is_hot` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否推荐',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  `sort_order` int(11) NOT NULL DEFAULT 100 COMMENT '排序',
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '描述',
  `template` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模板',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'URL',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_article
-- ----------------------------
INSERT INTO `hq_article` VALUES (1, 1, 2, '黄青', '', '', '', 'a:3:{i:0;s:53:\"http://img.ccpr.com.cn/2019/1122/103457_612832_60.jpg\";i:1;s:54:\"http://img.ccpr.com.cn/2019/1122/103457_612714_384.jpg\";i:2;s:54:\"http://img.ccpr.com.cn/2019/1122/103457_612785_936.jpg\";}', '<p>沙发发是打发沙发上发</p>', 0, 0, 0, 1, 100, '', '', 'show_2.phtml', '/show/1.html', 1574387469, 1574390104, 0);
INSERT INTO `hq_article` VALUES (2, 1, 2, '啊的沙发都是发  ', '', '', '', 's:0:\"\";', '<p>啊</p>', 0, 1, 0, 1, 100, '', '', 'show.phtml', '/show/2.html', 1574388507, 1574390172, 0);

-- ----------------------------
-- Table structure for hq_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `hq_auth_group`;
CREATE TABLE `hq_auth_group`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_auth_group
-- ----------------------------
INSERT INTO `hq_auth_group` VALUES (1, '超级管理员', '', 1, '6,44,43,2,84,89,102,101,90,1,94,95,97,96,11,25,26,27,56,57,58,59,98,99,4,12,92,45,68,69,70,103,5,16,37,38,39,17,40,41,42,15,22,23,24,18,53');

-- ----------------------------
-- Table structure for hq_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `hq_auth_group_access`;
CREATE TABLE `hq_auth_group_access`  (
  `uid` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `group_id` smallint(5) UNSIGNED NOT NULL DEFAULT 0
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限授权' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_auth_group_access
-- ----------------------------
INSERT INTO `hq_auth_group_access` VALUES (1, 1);
INSERT INTO `hq_auth_group_access` VALUES (27, 1);

-- ----------------------------
-- Table structure for hq_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `hq_auth_rule`;
CREATE TABLE `hq_auth_rule`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `icon` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `type` char(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'nav,auth',
  `index` tinyint(1) NOT NULL DEFAULT 0 COMMENT '快捷导航',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 104 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限规则' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_auth_rule
-- ----------------------------
INSERT INTO `hq_auth_rule` VALUES (1, 0, '文章管理', '', 'layui-icon-list', 3, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (2, 0, '会员管理', '', 'layui-icon-username', 2, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (4, 0, '系统设置', '', 'layui-icon-set', 5, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (5, 0, '管理员', '', 'layui-icon-auz', 6, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (6, 0, '控制台', 'admin/index/index', '', 1, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (11, 1, '广告管理', 'admin/ad/index', 'fa fa-image', 80, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (12, 4, '基本设置', 'admin/config/setting', 'fa fa-cog', 1, 'nav', 1, 1);
INSERT INTO `hq_auth_rule` VALUES (15, 5, '权限规则', 'admin/auth/rule', 'fa fa-th-list', 3, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (16, 5, '管理员', 'admin/admin/index', 'fa fa-user', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (17, 5, '权限组', 'admin/auth/group', 'fa fa-users', 1, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (18, 5, '管理员日志', 'admin/admin/log', 'fa fa-clock-o', 5, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (22, 15, '添加', 'admin/auth/addRule', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (23, 15, '编辑', 'admin/auth/editRule', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (24, 15, '删除', 'admin/auth/delRule', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (25, 11, '添加', 'admin/ad/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (26, 11, '编辑', 'admin/ad/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (27, 11, '删除', 'admin/ad/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (37, 16, '添加', 'admin/admin/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (38, 16, '编辑', 'admin/admin/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (39, 16, '删除', 'admin/admin/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (40, 17, '添加', 'admin/auth/addGroup', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (41, 17, '编辑', 'admin/auth/editGroup', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (42, 17, '删除', 'admin/auth/delGroup', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (43, 6, '修改密码', 'admin/index/editPassword', '', 2, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (44, 6, '清除缓存', 'admin/index/clear', '', 1, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (45, 4, '上传设置', 'admin/config/upload', 'fa fa-upload', 4, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (53, 18, '一键清空', 'admin/admin/truncate', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (56, 1, '友情链接', 'admin/link/index', 'fa fa-address-book-o', 99, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (57, 56, '添加', 'admin/link/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (58, 56, '编辑', 'admin/link/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (59, 56, '删除', 'admin/link/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (68, 45, '上传图片', 'admin/index/uploadimage', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (69, 45, '上传文件', 'admin/index/uploadfile', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (70, 45, '上传视频', 'admin/index/uploadvideo', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (84, 2, '用户管理', 'admin/user/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (89, 2, '商户管理', 'admin/shop/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (90, 2, '权限规则', 'admin/user_auth/rule', '', 10, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (92, 4, '参数设置', 'admin/config/param', '', 3, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (94, 1, '栏目管理', 'admin/category/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (95, 1, '文章管理', 'admin/article/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (96, 1, '模型管理', 'admin/category/models', '', 10, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (97, 1, '单页管理', 'admin/page/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (98, 0, '微信管理', 'admin/wechat/index', 'layui-icon-login-wechat', 4, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (99, 98, '微信用户', 'admin/wechat/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (101, 2, '权限组', 'admin/user_auth/group', '', 9, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (102, 2, '操作日志', 'admin/user/log', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (103, 4, '上传管理', 'admin/uploads/index', '', 10, 'nav', 0, 1);

-- ----------------------------
-- Table structure for hq_category
-- ----------------------------
DROP TABLE IF EXISTS `hq_category`;
CREATE TABLE `hq_category`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `userid` int(8) NULL DEFAULT NULL COMMENT '用户ID',
  `pid` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上级分类ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `catdir` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目录名称',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认头图',
  `sort_order` int(11) NOT NULL DEFAULT 100 COMMENT '排序',
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '描述',
  `model` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模型',
  `list_template` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列表页模板',
  `show_template` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容页模板',
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '栏目URL',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_category
-- ----------------------------
INSERT INTO `hq_category` VALUES (1, 1, 0, '关于我们', 'asdfasf', '', 100, '', '', 'page', 'list_111_3.phtml', 'show_2.phtml', 'https://vip.chunabao.cn/lists/1.html', 0, 0, 0);
INSERT INTO `hq_category` VALUES (2, NULL, 0, '新闻', '', '', 100, '', '', 'article', 'list_111.phtml', 'show.phtml', 'https://vip.chunabao.cn/lists/2.html', 0, 0, 0);
INSERT INTO `hq_category` VALUES (4, NULL, 0, '联系我们', '', '', 100, '', '', 'page', 'list_111.phtml', 'show.phtml', 'https://vip.chunabao.cn/lists/4.html', 0, 0, 0);

-- ----------------------------
-- Table structure for hq_category_model
-- ----------------------------
DROP TABLE IF EXISTS `hq_category_model`;
CREATE TABLE `hq_category_model`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `model` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模型',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板',
  `status` smallint(5) NULL DEFAULT 1 COMMENT '状态',
  `is_sys` smallint(5) NULL DEFAULT 0 COMMENT '是否系统',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(10) NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_category_model
-- ----------------------------
INSERT INTO `hq_category_model` VALUES (1, 'article', '文章模型', 1, 1, 1574347230, 1574347367, 0);
INSERT INTO `hq_category_model` VALUES (3, 'page', '单页模型', 1, 1, 1574347626, 1574351129, 0);

-- ----------------------------
-- Table structure for hq_link
-- ----------------------------
DROP TABLE IF EXISTS `hq_link`;
CREATE TABLE `hq_link`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userid` smallint(8) NULL DEFAULT NULL COMMENT '用户ID',
  `category` smallint(5) NOT NULL DEFAULT 0 COMMENT '分类',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '描述',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '链接',
  `target` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '打开方式',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片',
  `sort_order` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '广告' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_link
-- ----------------------------
INSERT INTO `hq_link` VALUES (1, NULL, 4, '12132', '111', '111', '_self', 'https://vip.chunabao.cn/uploads/20191122/609b85504372c224ef5a337dfaa7941c.jpg', 0, 1, 0, 0, 0);
INSERT INTO `hq_link` VALUES (2, NULL, 5, 'sfasdfas', '', '', '_self', '', 12, 1, 0, 0, 0);

-- ----------------------------
-- Table structure for hq_link_model
-- ----------------------------
DROP TABLE IF EXISTS `hq_link_model`;
CREATE TABLE `hq_link_model`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(10) NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_link_model
-- ----------------------------
INSERT INTO `hq_link_model` VALUES (4, '首页', 1574394319, 1574394319, 0);
INSERT INTO `hq_link_model` VALUES (5, '列表banner', 1574394336, 1574396950, 0);

-- ----------------------------
-- Table structure for hq_page
-- ----------------------------
DROP TABLE IF EXISTS `hq_page`;
CREATE TABLE `hq_page`  (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '图片',
  `photo` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '相册',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `view` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '点击量',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  `sort_order` int(11) NOT NULL DEFAULT 100 COMMENT '排序',
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '描述',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'URL',
  `template` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模板',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单页' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_page
-- ----------------------------
INSERT INTO `hq_page` VALUES (1, '关于我们', 'http://img.ccpr.com.cn/2019/1122/002252_590479_417.jpg', '', '<p>大师法士大夫</p>', 0, 1, 100, '', '', 1574352321, 1574353438, NULL, 'show_2.phtml');
INSERT INTO `hq_page` VALUES (4, '联系我们', '', NULL, NULL, 0, 1, 100, '', '', 1574351917, 1574351917, NULL, '');

-- ----------------------------
-- Table structure for hq_shop
-- ----------------------------
DROP TABLE IF EXISTS `hq_shop`;
CREATE TABLE `hq_shop`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商户名称',
  `contact` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mobile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rules` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '权限',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `exp_time` int(10) NULL DEFAULT NULL COMMENT '过期时间',
  `create_time` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_shop
-- ----------------------------
INSERT INTO `hq_shop` VALUES (1, '六诺科技', '黄青', '13055556455', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18', '没有备注', 0, 1574155664, 1574161909, 0);
INSERT INTO `hq_shop` VALUES (2, '新的公司', 'huangq', '13055577777', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18', '', 1574352000, 1574161505, 1574430805, 0);
INSERT INTO `hq_shop` VALUES (3, '12', '121212', '13055556455', NULL, NULL, 1577721600, 1574424456, 1574430943, 1574430943);
INSERT INTO `hq_shop` VALUES (4, '黄氏集团', '黄青', '13459401111', NULL, NULL, 0, 1574424598, 1574424598, 0);
INSERT INTO `hq_shop` VALUES (5, '1212', '1212', '13455555555', NULL, NULL, 1575043200, 1574424878, 1574424878, 0);

-- ----------------------------
-- Table structure for hq_uploads
-- ----------------------------
DROP TABLE IF EXISTS `hq_uploads`;
CREATE TABLE `hq_uploads`  (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `model` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户类型',
  `type` smallint(2) NULL DEFAULT NULL COMMENT '存储位置',
  `user_id` int(5) UNSIGNED NULL DEFAULT NULL COMMENT '用户ID',
  `filename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件名',
  `mine` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `filesize` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件大小',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'url',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 350 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_uploads
-- ----------------------------
INSERT INTO `hq_uploads` VALUES (344, 'admin', 1, 1, NULL, NULL, '187079', 'http://img.ccpr.com.cn/2019/1122/223805_391452_5.', 1574433485, 1574433485, 0);
INSERT INTO `hq_uploads` VALUES (345, 'admin', 1, 1, NULL, NULL, '187079', 'http://img.ccpr.com.cn/2019/1122/224726_648246_570.', 1574434046, 1574434046, 0);
INSERT INTO `hq_uploads` VALUES (346, 'admin', 1, 1, NULL, NULL, '187079', 'http://img.ccpr.com.cn/2019/1122/224750_513705_88.png', 1574434070, 1574434070, 0);
INSERT INTO `hq_uploads` VALUES (347, 'admin', 1, 1, 'home.png', 'image/png', '187079', 'http://img.ccpr.com.cn/2019/1122/224949_438246_201.png', 1574434189, 1574434189, 0);
INSERT INTO `hq_uploads` VALUES (348, 'admin', 1, 1, 'home.png', 'image/png', '187079', 'http://img.ccpr.com.cn/2019/1122/225224_945450_837.png', 1574434345, 1574435828, 1574435828);
INSERT INTO `hq_uploads` VALUES (349, 'admin', 1, 1, '256.png', 'image/png', '187079', 'http://img.ccpr.com.cn/2019/1122/225234_534440_240.png', 1574434354, 1574435889, 1574435889);

-- ----------------------------
-- Table structure for hq_user
-- ----------------------------
DROP TABLE IF EXISTS `hq_user`;
CREATE TABLE `hq_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `shop_id` smallint(8) NULL DEFAULT NULL COMMENT '商户ID',
  `mobile` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `avatarurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0禁用/1启动',
  `last_login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上次登录时间',
  `last_login_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '上次登录IP',
  `login_count` int(11) NOT NULL DEFAULT 0 COMMENT '登录次数',
  `sort_order` smallint(5) NULL DEFAULT 0 COMMENT '排序',
  `group_id` int(3) NULL DEFAULT NULL COMMENT '所属组',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10066 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_user
-- ----------------------------

-- ----------------------------
-- Table structure for hq_user_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `hq_user_auth_group`;
CREATE TABLE `hq_user_auth_group`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `shop_id` smallint(5) NULL DEFAULT NULL,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_user_auth_group
-- ----------------------------
INSERT INTO `hq_user_auth_group` VALUES (1, NULL, '超级管理员', '', 1, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18', 0, 0, 0);
INSERT INTO `hq_user_auth_group` VALUES (4, 4, '总经理', '', 1, '124,125,128,132,133,141,127,140,142,126,129,130,131,77,78,81,82,83,79,84,85,86,137,138,139,80,87,1,2,3', 0, 0, 0);
INSERT INTO `hq_user_auth_group` VALUES (5, 5, '总经理', '', 1, '124,125,128,132,133,141,127,140,142,126,129,130,131,77,78,81,82,83,79,84,85,86,137,138,139,80,87,1,2,3', 0, 0, 0);

-- ----------------------------
-- Table structure for hq_user_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `hq_user_auth_rule`;
CREATE TABLE `hq_user_auth_rule`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `icon` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `type` char(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'nav,auth',
  `index` tinyint(1) NOT NULL DEFAULT 0 COMMENT '快捷导航',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限规则' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_user_auth_rule
-- ----------------------------
INSERT INTO `hq_user_auth_rule` VALUES (1, 0, '记账管理', '', '', 0, 'nav', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (2, 1, '记账明细', 'user/finance/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (3, 1, '添加记账', 'user/finance/add', '', 0, 'nav', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (4, 1, '修改记账', 'user/finance/edit', '', 0, 'nav', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (5, 1, '删除记账', 'user/finance/del', '', 0, 'nav', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (6, 0, '报表管理', '', '', 0, 'nav', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (7, 6, '基本报表', 'user/report/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (8, 0, '用户设置', '', '', 0, 'nav', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (9, 8, '账号管理', 'user/staff/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (10, 9, '添加账号', 'user/staff/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (11, 9, '修改账号', 'user/staff/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (12, 9, '删除账号', 'user/staff/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (13, 8, '参数设置', 'user/config/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (14, 13, '添加参数', 'user/config/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (15, 13, '修改参数', 'user/config/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (16, 13, '删除参数', 'user/config/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (17, 8, '操作日志', 'user/log/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_user_auth_rule` VALUES (18, 17, '清空日志', 'user/log/clear', '', 0, 'auth', 0, 1);

-- ----------------------------
-- Table structure for hq_user_log
-- ----------------------------
DROP TABLE IF EXISTS `hq_user_log`;
CREATE TABLE `hq_user_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员id',
  `ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'ip地址',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求链接',
  `method` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求类型',
  `type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '资源类型',
  `param` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求参数',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '日志备注',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(10) NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 461 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for hq_wechat
-- ----------------------------
DROP TABLE IF EXISTS `hq_wechat`;
CREATE TABLE `hq_wechat`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '昵称',
  `gender` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '性别',
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '国家',
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '市',
  `avatarurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `unionid` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'UNIONID',
  `openid` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'OPENID',
  `sort_order` smallint(5) NULL DEFAULT 0 COMMENT '排序',
  `subscribe` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关注与否',
  `subscribe_time` int(10) NULL DEFAULT NULL COMMENT '关注时间',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11693 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_wechat
-- ----------------------------
INSERT INTO `hq_wechat` VALUES (11645, '黄青', '1', '中国', '福建', '福州', 'http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaEKF6OTJ8mUicyRl50jlRPMPtuloYwm4eo50yBXT2aXGficvqvibj6xmJI3wdYrPia2KCMlGI0rdgicibywA/132', NULL, 'o01-P5jawd6l0hFlEOuqFijxQQtk', 0, '1', 1554286127, 1574417942, 1574417942, 0);
INSERT INTO `hq_wechat` VALUES (11646, '旭亮', '1', '中国', '江苏', '徐州', 'http://thirdwx.qlogo.cn/mmopen/Q3auHgzwzM5phI29Svvnl5PQS5MBkl1msWkNdWE0mnMh0S4ibEwNWiawFq7mSu2HMvyytxjuN7icFtWAPibN2U4grQ/132', NULL, 'o01-P5sqofDOaoPq_4bwvuVCGfak', 0, '1', 1561561950, 1574417942, 1574417942, 0);
INSERT INTO `hq_wechat` VALUES (11647, 'Oct. Music，拾乐琴行🎸', '1', '中国', '山东', '潍坊', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1pMXD25EvC6vLjGRQwQpCDr8PwWicVZib26l1VYhIxiagrO5uDOAmG3NF4lfRAVIwulLcicib7u9sS9Ao4u0HklwIIS5/132', NULL, 'o01-P5p9tuck1Og7kwJDJ4NnXPuM', 0, '1', 1571646819, 1574417943, 1574417943, 0);
INSERT INTO `hq_wechat` VALUES (11648, 'W.', '2', '', '', '', 'http://thirdwx.qlogo.cn/mmopen/2rpPN8HphUEyzfSmcZCHtR5YjGbAIWIv10icYHDC4iaEalep2OZFxVtQsick3HLuubxcciauoDeeFrvPIlnVvYbicKgLXwFFDtLVB/132', NULL, 'o01-P5h9A63xR6OYDWbEJyS5CXBI', 0, '1', 1563778120, 1574417943, 1574417943, 0);
INSERT INTO `hq_wechat` VALUES (11649, '小庄', '1', '中国', '福建', '福州', 'http://thirdwx.qlogo.cn/mmopen/ajNVdqHZLLCOz6syXMiapfCs70Dnlj4m2vTyroNA3o6AVic0picDytGULGzAgu4eBiadQYf63Ep8UumIBhEibjsTyMQ/132', NULL, 'o01-P5tsfCh--s9PTN5_9bi76hFE', 0, '1', 1563094084, 1574417943, 1574417943, 0);
INSERT INTO `hq_wechat` VALUES (11650, '黄寒堂  夏歌琴行 珠江专卖 艾维尔', '1', '中国', '福建', '厦门', 'http://thirdwx.qlogo.cn/mmopen/ajNVdqHZLLDGwTJn3ASyEpgic4Ue6ywYLKBVrnbDjjXEQibY5JFEcLibe97Su8jRqAw0Sun0SE0u6ppH6SU1svaIA/132', NULL, 'o01-P5sLcpf4qsjEehX5gpA92Nco', 0, '1', 1554286843, 1574417943, 1574417943, 0);
INSERT INTO `hq_wechat` VALUES (11651, '袁小小小洁', '2', '冰岛', '', '', 'http://thirdwx.qlogo.cn/mmopen/XJvw5eibZMZooeuea6ibk1hQ5fN1Bpib2vpOngicBdZPpiahrXfkvPbrpiannzK2iao3nyfUuhDVJicGF6OuyuvmaapEPA/132', NULL, 'o01-P5glmBFPCS0hXC99xhPQhD-I', 0, '1', 1560156401, 1574417943, 1574417943, 0);
INSERT INTO `hq_wechat` VALUES (11652, '桃李钢琴教育-陈园园', '2', '中国', '河南', '信阳', 'http://thirdwx.qlogo.cn/mmopen/ajNVdqHZLLB75UavSIqGeIcibzI5uzycNnic3krFqORT86iclzmicRVzhD9qJWk2ApICZ1o7VVhuhVqlMlLWnm0f9Q/132', NULL, 'o01-P5praAJXK4jnNdg8ZgSH8cGM', 0, '1', 1563248092, 1574417944, 1574417944, 0);
INSERT INTO `hq_wechat` VALUES (11653, 'AIr.', '2', '法国', '里昂', '', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1oIghgonnXtr4P6w1RcjocibuCKEnW9Qzt6XaT3mXoHGKhIXx8FdnWO6Y6ibibYJxxicTNuMIb4azA2CA/132', NULL, 'o01-P5jtD1T4YLyNSK7MOM1CSV1o', 0, '1', 1571281754, 1574417944, 1574417944, 0);
INSERT INTO `hq_wechat` VALUES (11654, '筝艺阁', '2', '中国', '江苏', '泰州', 'http://thirdwx.qlogo.cn/mmopen/BmJHkvCeW9GiaDuuFTJ7ZrD7Oe86Lt1iarAM5HPlbtXQe8ibSlia0jwDKbp6frLcXWTqfha2e8zQww6gZ5dZrtcB3VIMTvwYNSz7/132', NULL, 'o01-P5hoMzf7vfFfu_6Q0aZWnBa0', 0, '1', 1560391059, 1574417944, 1574417944, 0);
INSERT INTO `hq_wechat` VALUES (11655, 'Cy xiao', '2', '中国', '', '', 'http://thirdwx.qlogo.cn/mmopen/BmJHkvCeW9FWXUWIVkEVNiaiccLgOJPtHUAATDYGnw48j9dtIfR65ic2uthEYdSSXjiaHeZJHqVQUIXcyI1QVacGMs7EDB9ibibyibj/132', NULL, 'o01-P5sLM26Lr2nQlEoFp90_0A-w', 0, '1', 1571461681, 1574417944, 1574417944, 0);
INSERT INTO `hq_wechat` VALUES (11656, 'Melo.', '1', '中国', '江苏', '徐州', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1q4s2l7icpf79qz40rPbSZM0UYtbwpaxicwCmXcWoFg1kosYEpIptPhSY1hKwu6eadAHF2AJcrtSfIxYYgmCTbsLA/132', NULL, 'o01-P5oS7EpY0pnWHHdqDKY67wS0', 0, '1', 1562320126, 1574417945, 1574417945, 0);
INSERT INTO `hq_wechat` VALUES (11657, '℡空白', '1', '中国', '福建', '福州', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1rWAKVI62JBX6yRicqclRzW4KHezRxzoCJGAQibFxPZCTMia0BUiaZbic6Scr3xcy9LibODS7jGmIiadfHG2BjrDC9stBI/132', NULL, 'o01-P5i1uHz6nhsC9PsbrVDgRiEU', 0, '1', 1556282063, 1574417945, 1574417945, 0);
INSERT INTO `hq_wechat` VALUES (11658, '林老师 艾维尔音乐名琴馆', '2', '中国', '福建', '福州', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1rnRQ3AWic3CvPI8E36mHRJPj7Jw6ltVVia2YQgK4HaAsXTIrKSQEWXib4e3MbIicWENa7BsfPKlb5afkxZricH7TDKm/132', NULL, 'o01-P5su3iwWalbv1DddC09clVSk', 0, '1', 1557884586, 1574417945, 1574417945, 0);
INSERT INTO `hq_wechat` VALUES (11659, '艾维尔名琴馆  调律  阿乐', '1', '中国', '福建', '福州', 'http://thirdwx.qlogo.cn/mmopen/k0Ue4mIpaVicgJePvRZfPpKHIks7UE7kcCVRZTauJZUibkHSRtKED6Jia2LqPFQ6icc24p72GFT9hNSt7m89quFPJ6icUTELonrSx/132', NULL, 'o01-P5k47esQ6RzEJVBlbaj5NsqY', 0, '1', 1559274888, 1574417945, 1574417945, 0);
INSERT INTO `hq_wechat` VALUES (11660, '艾维尔钢琴', '1', '中国', '福建', '福州', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1rTjXea7rcUC4z3BF8Rnm10TIVicYXrbCKvvxcNfrVUGDPfiatdyIQsk4ic3cB7kVYbP9icVLqxC2Lib4UlVjtjedSKb/132', NULL, 'o01-P5l4hqRzwhq3clu8kHsN9JqY', 0, '1', 1564105956, 1574417945, 1574417945, 0);
INSERT INTO `hq_wechat` VALUES (11661, 'Ann', '2', '中国', '北京', '', 'http://thirdwx.qlogo.cn/mmopen/BmJHkvCeW9HbaP1uEpGxAatqhqiceVbY36fnJEO8o8UA5nBLKqPiapnLYyz3ibia2t3ec8GBsq7RAs5eZkC8kWyAjCXric9sh87GG/132', NULL, 'o01-P5m3U-bYZT7gbQan89Rp5GTo', 0, '1', 1564191868, 1574417946, 1574417946, 0);
INSERT INTO `hq_wechat` VALUES (11662, 'A.🐳寓教于乐🎹武老师', '0', '', '', '', 'http://thirdwx.qlogo.cn/mmopen/SU31u8tkARc4CLYA6bibbklImzntVTwaJcEr6MX2BsyyDxMnC77sTkCIzPoheLYwweJYNmxDXWsvDjialJ8fRfHvhMysicAcgUJ/132', NULL, 'o01-P5qyk8Nkbt_i9PSTlcUV1vjQ', 0, '1', 1560913802, 1574417946, 1574417946, 0);
INSERT INTO `hq_wechat` VALUES (11663, '萝卜球', '2', '', '', '', 'http://thirdwx.qlogo.cn/mmopen/k0Ue4mIpaVicgJePvRZfPpDM267TVoAXPz4GicOE1tU2UFaOwibm2iaFQmX0phyrZhKicFgxB2b6EIc4KsYSKana1YhMY6Y9Tp5Dia/132', NULL, 'o01-P5l-9j2SKsMWk12AQrWeB3m4', 0, '1', 1561725726, 1574417946, 1574417946, 0);
INSERT INTO `hq_wechat` VALUES (11664, '真心不如红钞票', '1', '中国', '云南', '红河', 'http://thirdwx.qlogo.cn/mmopen/BmJHkvCeW9Em8Ja2DhLSyMTLYt4PohrMicnSeL90FvNdtbkUb0zQjGDd04BXRrEhaLHl0L6dVVzibORibvjPokzCWLx7kiawAxnz/132', NULL, 'o01-P5nYk3wRhvlYXlr5hbeX0ugo', 0, '1', 1564196574, 1574417946, 1574417946, 0);
INSERT INTO `hq_wechat` VALUES (11665, '忽而今夏', '0', '', '', '', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1rWAKVI62JBXyve4mc0GHYuO48IP4kicpG78KXuDvONFFvlQSspicspSEcEXQAX5vS8MIHc1dxyuGbSRWGMAfiabDp/132', NULL, 'o01-P5hiYUYDtouKVDJfLV5EHV9M', 0, '1', 1563259277, 1574417947, 1574417947, 0);
INSERT INTO `hq_wechat` VALUES (11666, '黃清', '1', '中国', '福建', '福州', 'http://thirdwx.qlogo.cn/mmopen/BmJHkvCeW9HbaP1uEpGxAU4YiawlASBcEB7jPZNJs0noAn5005evtep7Az8X2iarBJygZzjOic8BqwkPvtBymOjAumPWsIgo276/132', NULL, 'o01-P5g1nuFgcgR9Ju1SpjH8tF9M', 0, '1', 1556265417, 1574417947, 1574417947, 0);
INSERT INTO `hq_wechat` VALUES (11667, '钢琴·蒋老师🎹', '2', '中国', '北京', '朝阳', 'http://thirdwx.qlogo.cn/mmopen/XJvw5eibZMZpibfSJ30Zsicd2QSV2jTPLAnGf8DTGTRialXKT3GNfVq6KBic9iaOu3iafaQ3p2bynVqO7YhICmQWoRPq6OHevewRctJ/132', NULL, 'o01-P5lyNh2VhNk6-xvnPKeUxrc8', 0, '1', 1560998997, 1574417947, 1574417947, 0);
INSERT INTO `hq_wechat` VALUES (11668, '长高', '2', '法国', '巴黎', '', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1rWAKVI62JBXicp5X0RW21Ra1AYpp9frweUCzWibNic9YMSEt4oOia5eEGnib33OHyYChibQTYE3NkYpmeh2iaibmBy48NO/132', NULL, 'o01-P5iyNU_e30x8DVT5X0AoTgCU', 0, '1', 1571642008, 1574417947, 1574417947, 0);
INSERT INTO `hq_wechat` VALUES (11669, '🎵 陈毓婉🎵', '2', '', '', '', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1rzt1QEv9YA0MAQNaSVTgDLiauUPe49xcIbeefia88dLeZM20y2ULtanyZ1CRzoYxqXmfl78bYOGbFcDAASHSOKYx/132', NULL, 'o01-P5mIQs5Ul_DaAna1ZSZky0DE', 0, '1', 1570160725, 1574417947, 1574417947, 0);
INSERT INTO `hq_wechat` VALUES (11670, '好运来🔮', '2', '中国', '辽宁', '沈阳', 'http://thirdwx.qlogo.cn/mmopen/Q3auHgzwzM7ErxT3QgiaS71nlyY4XJcIyvUPt1WymRJSrvSUUeY6oQS5mwCIZLx5LW2lzRf4817axBM7micZVM1FFrYcPEGE2DwrvYJ7MRTnk/132', NULL, 'o01-P5u3LOi-LiiPoqxwskP1K6nU', 0, '1', 1563507543, 1574417948, 1574417948, 0);
INSERT INTO `hq_wechat` VALUES (11671, 'Say my name \'', '1', '安道尔', '', '', 'http://thirdwx.qlogo.cn/mmopen/BmJHkvCeW9HbaP1uEpGxAVayicqG34LWRicXVkfrEcaRGumnpl8d29uqBag1944RupYxRqhYib1icW1qzKRNOLLCuG903GiaAibNx1/132', NULL, 'o01-P5kCqIDhH3PerL8imcHaBSSo', 0, '1', 1571646791, 1574417948, 1574417948, 0);
INSERT INTO `hq_wechat` VALUES (11672, '◡̈⃝⃝', '2', '韩国', '首尔', '', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1rWAKVI62JBXicx0tMLdQN0xKgIocicMF7NyQv1s7ck75K7eSTeicBCUQBcnWcCst7AsWR6ZjSjXBz2XtxQVN93hg5/132', NULL, 'o01-P5sc2hkulObdkZNvgLJr6xJY', 0, '1', 1559274961, 1574417948, 1574417948, 0);
INSERT INTO `hq_wechat` VALUES (11673, '筱鹿   艾维尔名琴馆', '2', '', '', '', 'http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaEIgefxqm4MXUA4k4LMUwp45HNdpvv6mvrMbhDzaicS15ibNhcrNbMuGTB4xRicoiawOeE9juficxHnwUNWWibdxjQsJXVtsjBabA1tq4/132', NULL, 'o01-P5hgUmAecAJ-dnYGE3IaQwkA', 0, '1', 1561772235, 1574417948, 1574417948, 0);
INSERT INTO `hq_wechat` VALUES (11674, '杨京川的猪猪🐷', '2', '中国', '辽宁', '锦州', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1rWAKVI62JBX5GsuSKUibEj60GYblWoxgSkLItN0jTdicYt4sQZgtHpMT8b1ZZicL771ViaeBEJKQGGquESlH2BduAA/132', NULL, 'o01-P5uey0HBEgi0cK1ZlUa7ftjo', 0, '1', 1573184427, 1574417948, 1574417948, 0);
INSERT INTO `hq_wechat` VALUES (11675, '🦴', '2', '中国', '上海', '', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1oicpqXQJ040XQOyd7xFpVjaOXhWwkQCqzr9naRrDO6FLz4oHXbgoQriaBH7HPmyWGAwbpJBZINCsFRByH4QE63qr/132', NULL, 'o01-P5qeAKWTZ3MOa4wwe9oUlHIM', 0, '1', 1560342822, 1574417949, 1574417949, 0);
INSERT INTO `hq_wechat` VALUES (11676, '🤔', '1', '中国', '福建', '福州', 'http://thirdwx.qlogo.cn/mmopen/XJvw5eibZMZroIIbFsBofMAiahTMCacuZiaQHA5arZnoZlM8utf5Ca3H8B7ibrnUPrQiaFIhzvZHezgkYly164IKkGhlEmFkkesx6/132', NULL, 'o01-P5plJCtFLfyZGDBhbb1EPl7c', 0, '1', 1560148550, 1574417949, 1574417949, 0);
INSERT INTO `hq_wechat` VALUES (11677, '阿乐🤔', '1', '冰岛', '', '', 'http://thirdwx.qlogo.cn/mmopen/XJvw5eibZMZpibfSJ30Zsicd1CzDDiczZ68tFjJp9InIQ5oOho653bgutTyIvc5cGdvwjkIicsletqZDcG9dmgI9p8yPUf3gzF2CO/132', NULL, 'o01-P5oayxt_jA-ZnNynOb3NRNq4', 0, '1', 1559275161, 1574417949, 1574417949, 0);
INSERT INTO `hq_wechat` VALUES (11678, '蕊', '2', '中国', '天津', '滨海新区', 'http://thirdwx.qlogo.cn/mmopen/XJvw5eibZMZpibfSJ30ZsicdwJqN6yMVQibcEXQ6G2dR8khrl3b7UiaadfrBkAibVPgQGG440YtZdTA1RKg4xMibCC7jdKYN7dIzic5y/132', NULL, 'o01-P5kn-S4ON_JddMAOwGTasikE', 0, '1', 1563412032, 1574417949, 1574417949, 0);
INSERT INTO `hq_wechat` VALUES (11679, '冯东  艾维尔 名琴馆 🎹', '1', '安道尔', '', '', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1qHFwZcE3HRZOl3nMedfk3icQkIpbyIGSbjSVNTOYItMfdLuENrNNIq3TqmQL5mgZGFwSQxhajmyahOYoYv3ia5C9/132', NULL, 'o01-P5guKdxx1O0JDWHYBRkAkMEo', 0, '1', 1559275215, 1574417949, 1574417949, 0);
INSERT INTO `hq_wechat` VALUES (11680, 'Luck', '2', '爱尔兰', '劳斯', '', 'http://thirdwx.qlogo.cn/mmopen/ajNVdqHZLLCABIr6caYTicicMjKaSTib0FVv56j27EJibNtuIt1tj3HwHxZPgghEAYTREukyO0RCKfMX8ib00Libgljib9qSqYULQg83SmReY3kNxs/132', NULL, 'o01-P5j4iXLa8_XbWjjNJVIQHFVo', 0, '1', 1559275174, 1574417950, 1574417950, 0);
INSERT INTO `hq_wechat` VALUES (11681, '尚音琴行', '2', '中国', '福建', '福州', 'http://thirdwx.qlogo.cn/mmopen/BmJHkvCeW9HbaP1uEpGxAXx1hw1aI2NIKuvmEGOsOqDe1XrR3u1ic436c2JYybvdK7S18K7K7q7ZhzicGfibUxz3YLugWUiazUGn/132', NULL, 'o01-P5tbHTgTN7jwguvp5j5evZDA', 0, '1', 1573191059, 1574417950, 1574417950, 0);
INSERT INTO `hq_wechat` VALUES (11682, '🌺伊～泽😍', '2', '中国', '河南', '安阳', 'http://thirdwx.qlogo.cn/mmopen/BmJHkvCeW9HbaP1uEpGxAdGUckVFYTZ5wPEPVtLaeibXmN0ich3BYgrZlz5AicsV1M1bd3jwYXqr1xUibMS4uQlLibAweVCic1J2NC/132', NULL, 'o01-P5qkhyJQb3Rip02DKDCwSpuw', 0, '1', 1570338653, 1574417950, 1574417950, 0);
INSERT INTO `hq_wechat` VALUES (11683, '巴洛克', '1', '中国', '江苏', '徐州', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1rWAKVI62JBX2W1obhE5x3POYclqWesMb78KhdEP59RvCqweUws2seibvKq4lultmA3Hozrmf0MMcejhSwnkEgG1/132', NULL, 'o01-P5nhG9mksno6Ac3aI9b0J1LM', 0, '1', 1567045403, 1574417950, 1574417950, 0);
INSERT INTO `hq_wechat` VALUES (11684, '琴声', '1', '中国', '山东', '聊城', 'http://thirdwx.qlogo.cn/mmopen/SU31u8tkARcpXW4EeQgfPkXmY1rAiavGC8k4dY1NnfCmU9p9bGYOfPw1TLaYib7uyV9QghG9kkuDFpLcdOPMzwnjbiaQaWjQibQT/132', NULL, 'o01-P5kkFgnHQcfqEAcdNn83G6jY', 0, '1', 1564970326, 1574417950, 1574417950, 0);
INSERT INTO `hq_wechat` VALUES (11685, '艾维尔名琴馆🎹', '1', '中国', '福建', '福州', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1rSZXApYPnuyLEmbOica5552tuyAaqU32fTlMiaO6gbAxH9gcnYnIKwVZ6TDo6H7CmTzuNqA6fzEM4ibEQjNeYuyoF/132', NULL, 'o01-P5pMf5Q1E24ET2kH56Ecg83g', 0, '1', 1559281066, 1574417951, 1574417951, 0);
INSERT INTO `hq_wechat` VALUES (11686, 'smile', '2', '中国', '四川', '成都', 'http://thirdwx.qlogo.cn/mmopen/ICSxa9J6hfm0I8UW3U6Z6UichH3Aib6t39Ptj84ovDH7P8NDPia8iaTOed1vjCNoj9aAPyOJvsIy6kbaqrBb6VwciazwF2hod6P1j/132', NULL, 'o01-P5g-dqkVNZH0cFMa8Kza7H8U', 0, '1', 1566196209, 1574417951, 1574417951, 0);
INSERT INTO `hq_wechat` VALUES (11687, '沐雅琴行   瓜瓜', '2', '中国', '江苏', '宿迁', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1rWAKVI62JBXibypUr7fG66dicsnSylRiaWyHIHxcmiaYnzJjCee87dqXDqEiaYoKVH6vlOk5ZQMlvf6STwQFicWiaohO9/132', NULL, 'o01-P5poBEcf8pnEfDJCiBzLNidU', 0, '1', 1561102217, 1574417951, 1574417951, 0);
INSERT INTO `hq_wechat` VALUES (11688, '🍊 🍊 小西柚呀', '2', '智利', '', '', 'http://thirdwx.qlogo.cn/mmopen/Q3auHgzwzM4tRRoeut6ibuboE2yicicnQVcsv1icOldaWvvbswg8WGspFelpPd7UOdyyhSp7lRR0EQHPatS07ZdSg3cR7BIr8ocMd51avWuxULI/132', NULL, 'o01-P5pazw1Qsqj2Ekp6TYTzNu1I', 0, '1', 1558402817, 1574417951, 1574417951, 0);
INSERT INTO `hq_wechat` VALUES (11689, '海啸', '1', '中国', '福建', '厦门', 'http://thirdwx.qlogo.cn/mmopen/BmJHkvCeW9E1YYfb9Ukic45mVhKdjdZrvp1vTcTya5wH5956LrlkOaqd7W3N7uJib5aSTHcribzXwic0sIxib9kZ64R7vhRXOW7Mt/132', NULL, 'o01-P5ktgfUlENRPKVJklsmO2tLY', 0, '1', 1556282357, 1574417952, 1574417952, 0);
INSERT INTO `hq_wechat` VALUES (11690, '不LYNN', '2', '中国', '福建', '福州', 'http://thirdwx.qlogo.cn/mmopen/XJvw5eibZMZrYu9T2uFtfO3Dpm92A0dMZhFfZ5nZWFoAsMgHgUxcjOrVhPtQfyrCa0SGibyVGsnh34aDNcFTYZjibOA4znadQUW/132', NULL, 'o01-P5qLGwUU8PBl8f_W7LFrBcAA', 0, '1', 1569125124, 1574417952, 1574417952, 0);
INSERT INTO `hq_wechat` VALUES (11691, '丁烈争', '1', '奥地利', '上奥地利', '', 'http://thirdwx.qlogo.cn/mmopen/dgBw4MbJR1rWAKVI62JBXzdgQnd8DnjcEuUia0m77IiaeMRpraI0BaIwRawnCuibj4n9eribfNTr4tWGMictxYE7LGT36KObyXzgib/132', NULL, 'o01-P5otWOvWS0JRQUjHS0dsFKes', 0, '1', 1571060835, 1574417952, 1574417952, 0);
INSERT INTO `hq_wechat` VALUES (11692, '', '1', '中国', '浙江', '绍兴', 'http://thirdwx.qlogo.cn/mmopen/XJvw5eibZMZrSZJA1RH3RgW4I9iasMZktr8mGmXjbrdDqtvuVwZwQaRnicpmWQb2VadcWHiaUlbLjtwib3S5EDkMMeibVXY98bP9DK/132', NULL, 'o01-P5isDC__rxcQC7sQkJKnUomw', 0, '1', 1564969745, 1574417952, 1574417952, 0);

SET FOREIGN_KEY_CHECKS = 1;
