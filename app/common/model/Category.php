<?php
namespace app\common\model;

use think\Model;
use think\model\concern\SoftDelete;

class Category extends Model
{
    protected function _initialize()
    {
        parent::_initialize();
        
    }
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    protected $autoWriteTimestamp = true;
    protected $type = [

    ];
    protected function base($query)
    {
        //$query->where('userid', session('user_auth.user_id'));
    }
    public function models()
    {
        return $this->belongsTo(CategoryModel::class, 'model', 'model')->bind(['model_name'=>'name']);
    }
    public function page()
    {
        return $this->belongsTo(Page::class, 'id', 'id')->bind(['title','view','datetime'=>'create_time']);
    }
    static function update_url($id) {
        $col = self::where(['id'=>$id])->find();
        $url = request()->domain().'/lists/'.$col['id'].'.html';
        self::where(['id'=>$id])->update(['url'=>$url]);
        return;
    }
    //获取栏目列表
    static function list_to_level($model) {
        $list = list_to_level(self::where(['model'=>$model])->order('sort_order asc')->select());
        return $list;
    }
    //获取列表页模板
    static function list_template() {
        $filepath = app()->getRootPath().'view/'."index/" ."index" . DIRECTORY_SEPARATOR;
        $list = str_replace($filepath . DIRECTORY_SEPARATOR, '', glob($filepath . DIRECTORY_SEPARATOR . 'list*'));
        return $list;
    }
    //获取详情页模板
    static function show_template() {
        $filepath = app()->getRootPath().'view/'."index/" ."index" . DIRECTORY_SEPARATOR;
        $list = str_replace($filepath . DIRECTORY_SEPARATOR, '', glob($filepath . DIRECTORY_SEPARATOR . 'show*'));
        return $list;
    }
}