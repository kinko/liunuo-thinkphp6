<?php
namespace app\common\model;
use think\Model;
use think\model\concern\SoftDelete;
use think\facade\Config;

class Uploads extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    protected $autoWriteTimestamp = true;
    
    protected function base($query)
    {

    }
    public function getTypeAttr($value)
    {
        $status = [0=>'本地',1=>'七牛'];
        return $status[$value];
    }
    public function getFilesizeAttr($value)
    {
        return format_bytes($value);
    }
    static public function UploadValidate($file) {
        $upload_image = Config::load('admin/upload_setting');
        //$upload_image = config('upload_image');
        if ($upload_image['is_thumb'] == 1 || $upload_image['is_water'] == 1 || $upload_image['is_text'] == 1) {
            $object_image = \think\Image::open($file->getPathName());
            // 图片压缩
            if ($upload_image['is_thumb'] == 1) { 
                $object_image->thumb($upload_image['max_width'], $upload_image['max_height']);
            }
            // 图片水印
            if ($upload_image['is_water'] == 1) {
                $object_image->water(ROOT_PATH . str_replace('/', '\\', trim($upload_image['water_source'], '/')), $upload_image['water_locate'], $upload_image['water_alpha']);
            }
            // 文本水印
            if ($upload_image['is_text'] == 1) {
                $font = !empty($upload_image['text_font']) ? str_replace('/', '\\', trim($upload_image['text_font'], '/')) : 'vendor\topthink\think-captcha\assets\zhttfs\1.ttf';
                $object_image->text($upload_image['text'], ROOT_PATH . $font, $upload_image['text_size'], $upload_image['text_color'], $upload_image['text_locate'], $upload_image['text_offset'], $upload_image['text_angle']);
            }
            $object_image->save($file->getPathName());
        }
        return $file;
    }
}