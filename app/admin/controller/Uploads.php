<?php
namespace app\admin\controller;

use app\common\controller\AdminBase;
use think\facade\View;
use think\facade\Request;
use think\facade\Db;
use think\facade\Cache;
use think\facade\Session;
use think\facade\Env;
use think\facade\Filesystem;
use think\facade\Config;

use app\common\model\Uploads as Up;

class Uploads extends AdminBase
{
    protected $noAuth = [ 'uploadImage', 'uploadFile', 'uploadVideo'];
    
    public function index()
    {
        $list = Up::order('id desc')->paginate();
        return $this->fetch('index', ['list' => $list]);
    }
    
    
    //上传图片
    public function uploadImage()
    {
        try {
            $file = request()->file('file');
            //处理图片
            Up::UploadValidate($file);
            $params = Config::load('setting/qiniu','qiniu');
            //判断上传位置
            if($params['type']=='1'){//七牛
                $key = date('Y/md/His_').substr(microtime(), 2, 6).'_'.mt_rand(0,999).'.'.$file->getOriginalExtension();
                $qiniu = new \app\common\library\Qiniu();
                $url = $params['domain'].$qiniu->upload($file->getRealPath(), $key);
                Up::create([
                    'model' => 'admin',
                    'type' => '1',
                    'user_id'  => session('admin_auth.admin_id'),
                    'filename' => $file->getOriginalName(),
                    'filesize' => $file->getSize(),
                    'mine' => $file->getOriginalMime(),
                    'url'      =>  $url
                ]);
                insert_admin_log('上传了图片');
                return ['code' => 1, 'url' => $url,'msg'=>'上传成功'];
            }else{//默认本地
                $savename = Filesystem::disk('public')->putFile('uploads',$file);
                $url = request()->domain().'/'.$savename;
                Up::create([
                    'model' => 'admin',
                    'type' => '0',
                    'user_id'  => session('admin_auth.admin_id'),
                    'filename' => $file->getOriginalName(),
                    'filesize' => $file->getSize(),
                    'mine' => $file->getOriginalMime(),
                    'url'      =>  $url
                ]);
                insert_admin_log('上传了图片');
                return ['code' => 1, 'url' => $url,'msg'=>'上传成功'];
            }
        } catch (\Exception $e) {
            return ['code' => 0, 'msg' => $e->getMessage()];
        }
    }
    //删除图片
    public function del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $files = Up::where('id',$param['id'])->find();
            //判断存储位置
            if($files->getData('type')=='1'){
                $qiniuconfig = Config::load('setting/qiniu','qiniu');
                $key = str_replace($qiniuconfig['domain'],'',$files['url']);
                $qiniu = new \app\common\library\Qiniu();
                $qiniu->delete($key);
                Up::destroy($param['id']);
                insert_admin_log('删除了图片');
                $this->success('删除成功');
            }elseif($files->getData('type')=='0'){
                $this->success('暂不支持删除本地文件');
            }
            
        }
    }
    
}
