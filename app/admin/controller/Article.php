<?php
namespace app\admin\controller;
use app\common\controller\AdminBase;
use think\facade\View;
use think\facade\Request;
use think\facade\Db;
use think\facade\Cache;
use think\facade\Session;
use app\common\model\Article as Art;
use app\common\model\Category;


class Article extends AdminBase
{
    protected function _initialize()
    {
        parent::_initialize();
        $this->category = list_to_level(Category::where(['model'=>'article'])->order('sort_order asc')->select());
    }

    public function index()
    {
        $param = $this->request->param();
        $article = new Art();
        if (isset($param['title'])) {
            $article = $article->whereLike('title','%'.$param['title'].'%');
        }
        if (isset($param['cid'])) {
            $article = $article->where('cid',$param['cid']);
        }
        if (isset($param['is_top'])) {
            $article = $article->where('is_top',$param['is_top']);
        }
        if (isset($param['is_hot'])) {
            $article = $article->where('is_hot',$param['is_hot']);
        }
        if (isset($param['status'])) {
            $article = $article->where('status',$param['status']);
        }
        
        $list = $article->with('category')->order('id desc')->paginate();
        return $this->fetch('index', ['list' => $list,'category'=> Category::list_to_level('article')]);
    }

    public function add()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $param['userid'] = Session::get('admin_auth.admin_id');
            //验证规则
            $verify = input('_verify', true);
            if($verify!='0'){
                try{
                    $this->validate($param, 'article');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            $data = Art::create($param);
            if ($data == true) {
                $url = Art::update_url($data->id);
                insert_admin_log('添加了文章');
                $this->success('添加成功', url('@admin/article/index'));
            } else {
                $this->error($this->errorMsg);
            }
        }
        return $this->fetch('save',[
            'category'=> Category::list_to_level('article'),
            'show_template'=> Category::show_template()
        ]);
    }

    public function edit()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            //验证规则
            $verify = input('_verify', true);
            if($verify!='0'){
                try{
                    $this->validate($param, 'article');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            if (is_array($param['id'])) {
                $data = [];
                foreach ($param['id'] as $v) {
                    $data[] = ['id' => $v, $param['name'] => $param['value']];
                }
                $result = Art::saveAll($data);
            } else {
                $result = Art::update($param);
                $url = Art::update_url($param['id']);
            }
            if ($result == true) {
                insert_admin_log('修改了文章');
                $this->success('修改成功', url('@admin/article/index'));
            } else {
                $this->error($this->errorMsg);
            }
        }
        return $this->fetch('save', [
            'data' => Art::where('id', input('id'))->find(),
            'category'=> Category::list_to_level('article'),
            'show_template'=> Category::show_template()
        ]);
    }

    public function del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            Art::destroy($param['id']);
            insert_admin_log('删除了文章');
            $this->success('删除成功');
        }
    }
}
