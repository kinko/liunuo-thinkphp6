<?php
namespace app\admin\controller;
use app\common\controller\AdminBase;
use think\facade\Db;
use think\facade\Request;
use think\facade\Session;
use think\facade\View;
use think\facade\Cookie;
use app\common\model\Category as Cg;
use app\common\model\CategoryModel;
use app\common\model\AuthRule;
use app\common\model\Page;
use think\Model;

class Category extends AdminBase
{
    public function _initialize()
    {
        parent::_initialize();
        
    }
    //首页
    public function index()
    {
        return $this->fetch('index', ['list' => list_to_level(Cg::with(['models'])->order('sort_order asc')->select())]);
    }
    //新增分类
    public function add()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            //验证规则
            $verify = input('_verify', true);
            if($verify!='0'){
                try{
                    $this->validate($param, 'category');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            //添加
            $data = Cg::create($param);
            if ($data == true) {
                $url = Cg::update_url($data->id);
                //单页处理
                Page::update_page($param['model'],$data->id,$param['name']);
                insert_admin_log('添加了分类');
                $this->success('添加成功', url('@admin/category/index'));
            } else {
                $this->error($this->errorMsg);
            }
        }
        View::assign('models', CategoryModel::lists());
        return $this->fetch('save', [
            'category' => list_to_level(Cg::order('sort_order asc')->select()),
            'list_template' => Cg::list_template(),
            'show_template' => Cg::show_template()
        ]);
    }

    public function edit()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            //验证规则
            $verify = input('_verify', true);
            if($verify!='0'){
                try{
                    $this->validate($param, 'category');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            //添加
            $data = Cg::update($param);
            if ($data == true) {
                $url = Cg::update_url($param['id']);
                //单页处理
                Page::update_page($param['model'],$param['id'],$param['name']);
                insert_admin_log('修改了分类');
                $this->success('修改成功', url('@admin/category/index'));
            } else {
                $this->error($this->errorMsg);
            }
        }
        View::assign('models', CategoryModel::lists());
        return $this->fetch('save', [
            'data'     => Cg::where('id', input('id'))->find(),
            'category' => list_to_level(Cg::order('sort_order asc')->select()),
            'list_template' => Cg::list_template(),
            'show_template' => Cg::show_template()
        ]);
    }

    public function del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            Cg::destroy($param['id']);
            insert_admin_log('删除了分类');
            $this->success('删除成功');
        }
    }
    
    
    //模型管理
    public function models()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            extraconfig($param, 'website');
            $this->success('设置成功');
        }
        $list = CategoryModel::select();
        return $this->fetch('models', ['list' => $list]);
    }
    //模型添加
    public function models_add()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            //验证规则
            $verify = input('_verify', true);
            if($verify!='0'){
                try{
                    $this->validate($param, 'category_model');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            $data = CategoryModel::create($param);
            if ($data == true) {
                insert_admin_log('添加了模型');
                //新增权限
                AuthRule::create([
                    'pid'         =>  '1',
                    'name'        =>  $param['name'],
                    'url'         =>  'admin/page/'.$param['model'],
                    'sort_order'  => '0',
                    'type'        => 'nav',
                    'index'       => '0',
                    'status'      => '1' 
                ]);
                $this->success('添加成功', url('@admin/category/models'));
            } else {
                $this->error($this->errorMsg);
            }
        }
        return $this->fetch('models_save');
    }
    //模型编辑
    public function models_edit()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            //验证规则
            $verify = input('_verify', true);
            if($verify!='0'){
                try{
                    $this->validate($param, 'category_model');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            $data = CategoryModel::where('id',$param['id'])->find();
            if($data['is_sys']=='1'){
                $this->error('系统模型，禁止修改');
            }
            $data = CategoryModel::update($param);
            if ($data == true) {
                insert_admin_log('修改了模型');
                $this->success('修改成功', url('@admin/category/models'));
            } else {
                $this->error($this->errorMsg);
            }
        }
        return $this->fetch('models_save',['data'=> CategoryModel::where('id', input('id'))->find()]);
    }
    //模型删除
    public function models_del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $data = CategoryModel::where('id',$param['id'])->find();
            if($data['is_sys']=='1'){
                $this->error('系统模型，禁止删除');
            }else{
                CategoryModel::destroy($param['id']);
                insert_admin_log('删除了模型');
                $this->success('删除成功');
            }
        }
    }
    
}
